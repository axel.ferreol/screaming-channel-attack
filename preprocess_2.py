import sys
import os
from os import path, system

import numpy as np
from matplotlib import pyplot as plt
from scipy import signal

def shift_trace(trace, shift):
    length = len(trace)
    trace_aligned = np.zeros((length), dtype = np.float32)
    if  shift < 0:
        shift = length + shift 
    trace_aligned[:length - shift] = trace[shift:]
    trace_aligned[length - shift:] = trace[:shift]
    return trace_aligned


profile_traces_path = "" # repertoire des 25 000 traces pour le profile
open_path = "" #repertoire des 2 500 traces collectees pour l attaque
save_path = "" #repertoire de sauvegarde des 2 500 traces pour l attaque


num_points_profile = 50000
traces = []
for index in range(num_points_profile):
    traces.append( np.load(path.join(profile_traces_path, 'avg__%d.npy' %(index) ) ) )
template = np.average(traces, axis=0)


num_points_attack = 25000

for index in range(num_points_attack):
	if index % 500 == 0:
		print "Nb processed= %d" % index
        trace = np.load(path.join(open_path, 'avg__%d.npy' %(index) ) )

        correlation = signal.correlate(trace**2, template**2)
        shift = np.argmax(correlation) - (len(template)-1)
        trace = shift_trace(trace, shift)

        np.save(path.join(save_path, 'avg__%d.npy' % (index) ), trace) 

