from re import TEMPLATE
from os import path

import numpy as np
from matplotlib import pyplot as plt
from scipy import signal

def shift_trace(trace, shift):
    length = len(trace)
    trace_aligned = np.zeros((length), dtype = np.float32)
    if  shift < 0:
        shift = length + shift 
    trace_aligned[:length - shift] = trace[shift:]
    trace_aligned[length - shift:] = trace[:shift]
    return trace_aligned

num_points = 25000 #number of traces
precision = 100 #interval of prints

open_path = "" #path to the traces
save_path = "" #path to save preprocessed traces
template_path = "" #path to save the template


for index in range(num_points):      
    trace = np.load(path.join(open_path,"avg__{}.npy".format(index)))
    
    if index < 1:
        template=trace        
        np.save(path.join(save_path, "avg__{}.npy".format(index)), trace)
        continue
    
    else:              
        correlation = signal.correlate(trace**2, template**2)
        shift = np.argmax(correlation) - (len(trace)-1)
        trace = shift_trace(trace, shift)
        np.save(path.join(save_path, "avg__{}.npy".format(index)), trace)
        template = (trace + (float(index))*template)/(float(index+1))

    if index % precision == 0:
        print("Nb processed={}".format(index))

np.save(template_path,template)